#ids were replaced because they are hard coded values due to outstanding issues with the recent release of discord.py patch.
#token is also missing for my convenience.
import discord
import time
from discord.ext import commands

intents = discord.Intents.all()

client = discord.Client(intents=intents)

@client.event
async def on_ready():
	print('Bot is ready.')

@client.event
async def on_member_join(member):
	time.sleep(10)
	welcome_channel = discord.utils.get(member.guild.channels, id=channel__id)
	please_read_channel = discord.utils.get(member.guild.channels, id=channel__id)
	rules_channel = discord.utils.get(member.guild.channels, id=channel__id)
	
	await welcome_channel.send(f'Welcome {member.mention}, please read {rules_channel.mention} and {please_read_channel.mention}. Once you have read the channels, please leave a 👍 reaction on the {rules_channel.mention}')


@client.event
async def on_member_remove(member):
	goodbye_channel = discord.utils.get(member.guild.channels, id=channel__id) 
	await goodbye_channel.send(f'{member.display_name} has left the server.')

@client.event
async def on_raw_reaction_add(payload):
	welcome_channel = discord.utils.get(payload.member.guild.channels, id=channel__id)
	rules_channel = discord.utils.get(payload.member.guild.channels, id=channel__id)

	name = ''
	if payload.member.nick != None:
		name = payload.member.nick
	else:
		name = payload.member.name
	if payload.emoji.name == '👍':
		if payload.channel_id == rules_channel.id:
			await welcome_channel.send(f'{name} has agreed to the guild rules. Please change your nickname to main-toon-name(legacy-name)')


client.run(bot_token)
