from urllib.request import urlopen as uReq
from bs4 import BeautifulSoup as soup

##This script will 1.Accesss the URL below. 2. go through each of the links in a specific container and pull specific details from each page. 3. Output them in a way that would make the details easy to insert into a mySQL database.
my_url = 'https://www.crunchyroll.com/videos/anime/alpha?group=all'

#open connection and grab page
uClient = uReq(my_url)
page_html = uClient.read()
uClient.close()

#html parser
page_soup = soup(page_html, "html.parser")

#grab each item title
containers = page_soup.findAll("div", {"class":"videos-column-container"})

filename = 'crunchyroll.txt'
f = open(filename, "a", encoding="utf-8")

for div in containers:
	links = div.findAll('a')
	for a in links:
		uClient = uReq('https://www.crunchyroll.com' + a['href'])
		page_html = uClient.read()
		uClient.close()

		page_soup = soup(page_html, "html.parser")
		#find main info
		prop_name = page_soup.findAll("span", {"itemprop":"name"})
		name = prop_name[0].text
		prop_description = page_soup.findAll("span", {"class":"trunc-desc"})
		description = prop_description[0].text
		f.write('\n')
		f.write('\n')
		f.write('\n')
		f.write('Name: ' + name)
		f.write('\n')
		f.write('Description: ' + description)
		f.write('\n')

		#find info in sidebar
		sidebar = page_soup.findAll("div", {"class":"right"})
		for li in sidebar:
			prop_tag = page_soup.findAll("a", {"class":"text-link"})
			for tag in prop_tag:
				tag = tag.text
				f.write('Tag: ' + tag)
				f.write('\n')

			prop_detail = page_soup.findAll("li", {"class":None})
			for detail in prop_detail:
				detail = detail.text
				f.write('Detail: ' + detail)
				f.write('\n')

			
f.close()

exit()
